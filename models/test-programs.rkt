#lang racket

(require redex)
(require racket/include)
(require "obliv-lambda.rkt")

;; INSTRUCTIONS:

;; This file imports everything from obliv-lambda, and provides some examples of the usage of various languages.
;; All provided languages have complete syntax, along with complete typing judgments and a complete set of reduction relations.
;; The four languages are:

;; 1: lambda-typed: a simply typed lambda calculus with booleans and integers, and prefix notation for its operators, as well as a CBN conditional statement taking a boolean condition.
;; 2: obliv-lambda: an extension of lambda-typed which adds obliv data types and an oblivif construction that implements secret conditionals
;; 3: lambda-mem: and extension of lambda-typed that adds mutable memory via a pointer like construction, along with a sequence operator and a pointer type annotation
;; 4: obliv-lambda-mem: and extension of lambda-mem that adds obliv data types, along with oblivif, oblivset, and ~obliv constructions, and a frozen type annotation

;; For convenience, each language is also accompanied by four helper functions, which take a (term <expression>), where the expression is in the languages syntax,
;; and perform the desired operation upon that expression, setting up the appropriate program context (e.g. an empty σ or η) as necessary. Those helper functions are as follows:

;; 1: judge-lambda, derive-lambda, trace-lambda, reduce-lambda
;; 2: judge-obliv, derive-obliv, trace-obliv, reduce-obliv
;; 3. judge-mem, derive-mem, trace-mem, reduce-mem
;; 4. judge-obliv-mem, derive-obliv-mem, trace-obliv-mem, reduce-obliv-mem



;; EXAMPLES:

; (judge-obliv-mem (term ((λ (x_1 (obliv bool)) ((λ (x_2 (ptr (obliv int))) ((λ (x_3 int) ((λ (x_4 (ptr int)) (oblivif x_1 (seq (~obliv x_5 (set x_4 x_3)) (oblivset x_2 (- x_3 2))) (oblivset x_2 (+ x_3 3)))) (ref 0))) 3)) (ref 0))) #t)))
;; result: (obliv int)

; (show-derivations (derive-obliv-mem (term ((λ (x_1 (obliv bool)) ((λ (x_2 (ptr (obliv int))) ((λ (x_3 int) ((λ (x_4 (ptr int)) (oblivif x_1 (seq (~obliv x_5 (set x_4 x_3)) (oblivset x_2 (- x_3 2))) (oblivset x_2 (+ x_3 3)))) (ref 0))) 3)) (ref 0))) #t))))
;; prints proof of previous result

; (judge-obliv-mem (term ((λ (x_1 (obliv bool)) ((λ (x_2 (ptr (obliv int))) ((λ (x_3 int) ((λ (x_4 (ptr int)) (oblivif x_1 (seq (set x_4 x_3) (oblivset x_2 (- x_3 2))) (oblivset x_2 (+ x_3 3)))) (ref 0))) 3)) (ref 0))) #t)))
;; result: () i.e. this should not type check

; (reduce-obliv-mem (term ((λ (x_1 (obliv bool)) ((λ (x_2 (ptr (obliv int))) ((λ (x_3 int) ((λ (x_4 (ptr int)) (oblivif x_1 (seq (~obliv x_5 (set x_4 x_3)) (oblivset x_2 (- x_3 2))) (oblivset x_2 (+ x_3 3)))) (ref 0))) 3)) (ref 0))) #t)))
;; result: (((σ (l1 1) (l2 3)) (η #t) 1))

; (trace-obliv-mem (term ((λ (x_1 (obliv bool)) ((λ (x_2 (ptr (obliv int))) ((λ (x_3 int) ((λ (x_4 (ptr int)) (oblivif x_1 (seq (~obliv x_5 (set x_4 x_3)) (oblivset x_2 (- x_3 2))) (oblivset x_2 (+ x_3 3)))) (ref 0))) 3)) (ref 0))) #t)))
;; prints reduction trace of previous

; (reduce-lambda (term (,fib 10)))
;; result: (34)

; (reduce-obliv (term (,fib 10)))
;; result: (34)

; (reduce-mem (term (,fib 10)))
;; result: (((σ) 34))

; (reduce-obliv-mem (term (,fib 10)))
;; result: (((σ) (η #t) 34))

; (reduce-obliv (term (,ofib 10)))
;; result: does not terminate

; (reduce-mem (term (,memfib 10)))
;; result: (((σ (l1 34) (l2 21) (l3 21)) 34))




;; Some programs used by the examples

(define fib (term (fix (λ (f (→ int int)) (λ (x int) (if (= x 1) 0 (if (= x 2) 1 (+ (f (- x 1)) (f (- x 2))))))))))

;; diverges
(define ofib (term (fix (λ (f (→ (obliv int) (obliv int))) (λ (x (obliv int)) (oblivif (= x 1) 0 (oblivif (= x 2) 1 (+ (f (- x 1)) (f (- x 2))))))))))

(define memfib (term (λ (x_5 int) ((((λ (x_1 (ptr int)) (λ (x_2 (ptr int)) (λ (x_3 (ptr int))
      ((fix (λ (f (→ int int)) (λ (x_4 int) (if (= x_4 1) 0 (if (= x_4 2) (seq (set x_1 1) (deref x_1)) (seq (f (- x_4 1)) (set x_2 (deref x_1)) (set x_1 (+ (deref x_1) (deref x_3))) (set x_3 (deref x_2)) (deref x_1))))))) x_5)  ))) (ref 0)) (ref 0)) (ref 0)))))